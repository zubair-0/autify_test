# autify_test

## Getting Started
To solve the problem, I would design the solution in the following way:

- Use `argparse` library to handle command line arguments 
- Use `requests` library to fetch web pages 
- Use `BeautifulSoup` library to parse HTML pages and extract metadata 
- Use `datetime` library to record the date and time of last fetch 
- Use `os` library to check if the directory exists or create a new directory 
- Use `logging` library to log errors and events 
- Use `unittest` library to write tests 
- Use `docker` to containerize the application

### fetcher.py
The fetcher.py script defines the Fetcher class, which encapsulates the logic for fetching web pages and 
recording metadata. The script takes in command line arguments using the argparse library and uses the 
requests and BeautifulSoup libraries to fetch and parse the web pages. It also uses the logging library 
to log errors and events instead of printing them to the console.

### fetch.sh
The fetch shell script simply invokes the fetcher.py script with the command line arguments passed to it.

### How to run the program using Docker
Finally, to build and run the program using Docker, we can define a Dockerfile as follows: We can then build the Docker image using `docker build -t fetch`

### How to run
In Section 1, we can invoke the program by running `./fetch https://www.google.com https://autify.com`. The program will fetch the pages for both URLs and save them to the current directory as www.google.com.html and autify.com.html.

If the program runs into any errors while fetching the pages, it will log the error to the console.

In Section 2, the program records metadata about the fetched pages, including the URL, file name, date and time of last fetch, number of links, and number of images. It saves the metadata to a file called metadata.txt in the output directory.

### How to run tests
To run the tests for the program, we can use the unittest library to define test cases and assertions for the Fetcher class which is exactly what we did in the `fetcher_test.py`.
