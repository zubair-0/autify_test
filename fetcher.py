import argparse
import datetime
import os
import requests
from bs4 import BeautifulSoup
import logging

logging.basicConfig(level=logging.INFO)


class Fetcher:
    """Encapsulates the logic for fetching web pages and recording metadata"""
    def __init__(self, urls, output_dir, archive_assets=False):
        self.urls = urls
        self.output_dir = output_dir
        self.archive_assets = archive_assets

    def fetch(self):
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        for url in self.urls:
            try:
                response = requests.get(url)
                response.raise_for_status()

                soup = BeautifulSoup(response.content, 'html.parser')

                title = soup.title.string.strip() if soup.title else 'untitled'
                output_file = os.path.join(self.output_dir, title + '.html')

                with open(output_file, 'wb') as f:
                    f.write(response.content)

                self.record_metadata(url, output_file, soup)

                if self.archive_assets:
                    self.archive(url, soup)

            except requests.exceptions.RequestException as e:
                logging.error(f"Error fetching {url}: {e}")
            except Exception as e:
                logging.error(f"Error processing {url}: {e}")

    def record_metadata(self, url, output_file, soup):
        metadata_file = os.path.join(self.output_dir, 'metadata.txt')
        with open(metadata_file, 'a') as f:
            f.write(f"URL: {url}\n")
            f.write(f"File: {output_file}\n")
            f.write(f"Last fetch: {datetime.datetime.now().isoformat()}\n")
            f.write(f"Links: {len(soup.find_all('a'))}\n")
            f.write(f"Images: {len(soup.find_all('img'))}\n\n")

    def archive(self, url, soup):
        for link in soup.find_all('link'):
            self.download_asset(url, link['href'])
        for script in soup.find_all('script'):
            self.download_asset(url, script['src'])
        for img in soup.find_all('img'):
            self.download_asset(url, img['src'])

    def download_asset(self, url, asset_url):
        try:
            response = requests.get(asset_url)
            response.raise_for_status()

            output_dir = os.path.join(self.output_dir, 'assets', url.split('://')[1])
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            output_file = os.path.join(output_dir, os.path.basename(asset_url))

            with open(output_file, 'wb') as f:
                f.write(response.content)

        except requests.exceptions.RequestException as e:
            logging.error(f"Error fetching asset {asset_url} for {url}: {e}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Fetch web pages and record metadata')
    parser.add_argument('urls', metavar='url', type=str, nargs='+',
                        help='the URLs to fetch')
    parser.add_argument('--output-dir', dest='output_dir', type=str, default='.',
                        help='the output directory to save the fetched pages')
    parser.add_argument('--archive', dest='archive_assets', action='store_true',
                        help='archive all assets referenced in the HTML pages')
    args = parser.parse_args()

    fetcher = Fetcher(args.urls, args.output_dir, args.archive_assets)
    fetcher.fetch()
