FROM python:3

WORKDIR /app

COPY fetcher.py .
COPY fetch .

RUN chmod +x fetch

RUN pip install requests beautifulsoup4

ENTRYPOINT ["./fetch"]
