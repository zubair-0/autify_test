import unittest
import os
import shutil
from fetcher import Fetcher

class TestFetcher(unittest.TestCase):
    def setUp(self):
        self.urls = ['https://www.google.com', 'https://autify.com']
        self.output_dir = 'test_output'
        self.archive_assets = True
        self.fetcher = Fetcher(self.urls, self.output_dir, self.archive_assets)

    def tearDown(self):
        shutil.rmtree(self.output_dir)

    def test_fetch(self):
        self.fetcher.fetch()
        self.assertTrue(os.path.exists(os.path.join(self.output_dir, 'www.google.com.html')))
        self.assertTrue(os.path.exists(os.path.join(self.output_dir, 'autify.com.html')))

    def test_record_metadata(self):
        soup = BeautifulSoup('<html><head><title>Test page</title></head><body><a href="https://www.google.com">Google</a><img src="https://www.google.com/images/logo.png"></body></html>', 'html.parser')
        self.fetcher.record_metadata('https://www.example.com', os.path.join(self.output_dir, 'test.html'), soup)
        with open(os.path.join(self.output_dir, 'metadata.txt')) as f:
            metadata = f.read()
        self.assertIn('URL: https://www.example.com\n', metadata)
        self.assertIn('File: test.html\n', metadata)
        self.assertIn('Links: 1\n', metadata)
        self.assertIn('Images: 1\n', metadata)

    def test_archive(self):
        soup = BeautifulSoup('<html><head><title>Test page</title><link rel="stylesheet" href="style.css"><script src="script.js"></script></head><body><img src="image.jpg"></body></html>', 'html.parser')
        self.fetcher.archive('https://www.example.com', soup)
        self.assertTrue(os.path.exists(os.path.join(self.output_dir, 'assets', 'www.example.com', 'style.css')))
        self.assertTrue(os.path.exists(os.path.join(self.output_dir, 'assets', 'www.example.com', 'script.js')))
        self.assertTrue(os.path.exists(os.path.join(self.output_dir, 'assets', 'www.example.com', 'image.jpg')))

if __name__ == '__main__':
    unittest.main()
